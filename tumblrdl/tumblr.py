#!/usr/bin/env python

import argparse
import io
import json
import os
import sys
from functools import partial
from multiprocessing.pool import ThreadPool

import requests
import math
from urllib.parse import urlparse
from colorama import init

from peewee import *
from colorama import Fore, Back, Style
import datetime

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf8')
database_proxy = Proxy()
# import logging
# logging.basicConfig(
#     format='[%(asctime)-15s] [%(name)s] %(levelname)s]: %(message)s',
#     level=logging.DEBUG
# )

# 1: Photos
# 2: Video
# 3: Audio
# 4: Text


class BaseModel(Model):
    class Meta:
        database = database_proxy


class PostModel(BaseModel):
    postid = IntegerField(primary_key=True, index=True)
    timestamp = IntegerField()
    posturl = TextField()
    date = TextField()
    posttype = IntegerField()
    tags = TextField()
    contentfield1 = TextField(null=True)
    contentfield2 = TextField(null=True)
    otherfield = TextField(null=True)


class PostContentModel(BaseModel):
    post = ForeignKeyField(PostModel, related_name='post_model', index=True)
    posttype = IntegerField()
    downloaded = BooleanField(default=False)
    url = TextField(index=True, unique=True)
    filename = TextField(index=True)


def args_handler(argv):
    p = argparse.ArgumentParser(
            description='download from tumblr.com')
    p.add_argument('commands', type=str, nargs='*', help='Commands')
    p.add_argument('-k', '--appkey', action='store', type=str,
                   default='fuiKNFp9vQFvjLNvx4sUwti4Yb5yGutBN4Xh10LXZhhRKjWlV4',
                   help='Tumblr Api key, register at http://www.tumblr.com/oauth/apps')
    p.add_argument('-pt', '--posttype', action='store', type=str, default='photo',
                   help='Download post type, could be \'photo\', \'video\', \'audio\'')
    p.add_argument('-w', '--workers', action='store', type=int, default=10,
                   help='Number of worker threads, default 10')
    p.add_argument('-o', '--offset', action='store', type=int, default=0,
                   help='offset')
    p.add_argument('-q', '--quiet', action='store_true',
                   help='quiet')
    p.add_argument('-f', '--force', action='store_true',
                   help='force')
    p.add_argument('-t', '--tag', action='store',
                   default=None, type=str,
                   help='tag, eg: -t beautiful')
    args = p.parse_args(argv[1:])
    commands = args.commands

    return args, commands


def init_repo(appkey, blogname, repo_path, posttype):
    if os.path.exists(repo_path):
        print('Directory already exists.')
        exit()
    repo_config_path = os.path.join(repo_path, '.tumblr')
    if not os.path.exists(repo_config_path):
        os.makedirs(repo_config_path)

    db_path = os.path.join(repo_config_path, 'data.db')
    config_file_path = os.path.join(repo_config_path, 'config.json')

    database = SqliteDatabase(db_path, journal_mode='WAL')
    database_proxy.initialize(database)

    PostModel.create_table()
    PostContentModel.create_table()

    repo_configs = {
        'appkey': appkey,
        'tumblr_name': blogname,
        'post_type': posttype
    }
    with open(config_file_path, 'w') as configfile:
        json.dump(repo_configs, configfile)

    if posttype == 'video':
        os.makedirs(os.path.join(repo_path, "video"))
    elif posttype == 'photo':
        os.makedirs(os.path.join(repo_path, "photo"))
    elif posttype == 'audio':
        os.makedirs(os.path.join(repo_path, "audio"))
    else:
        os.makedirs(os.path.join(repo_path, "video"))
        os.makedirs(os.path.join(repo_path, "photo"))
        os.makedirs(os.path.join(repo_path, "audio"))

    print('Init repo for {0} done.'.format(blogname))


def status_repo(repo_path, workers, timeout):
    db_path, configdata = get_repo_config(repo_path)
    appkey = configdata['appkey']
    blogname = configdata['tumblr_name']

    database = SqliteDatabase(db_path, journal_mode='WAL')
    database_proxy.initialize(database)

    proxies = {}
    if 'proxies' in configdata:
        proxies = configdata['proxies']
        print(proxies)

    blog_info_url_base = 'http://api.tumblr.com/v2/blog/{0}.tumblr.com/info?api_key={1}'
    blog_info_url = blog_info_url_base.format(blogname, appkey)
    inforesponse = requests.get(blog_info_url, proxies)
    response_info = inforesponse.json()
    if response_info['meta']['msg'] != 'OK' or response_info['meta']['status'] != 200:
        print('Response error: ' + response_info['meta']['msg'])
        exit()
    blog_json = response_info['response']['blog']
    blog_title = ''
    if 'title' in blog_json:
        blog_title = blog_json['title']
    else:
        blog_title = blog_json['name']
    blog_url = blog_json['url']
    blog_last_update = datetime.datetime.fromtimestamp(blog_json['updated'])
    blog_posts = blog_json['posts']
    local_downloaded_posts_content = PostContentModel.select().where(PostContentModel.downloaded).count()
    local_all_posts_content = PostContentModel.select().count()
    local_posts = PostModel.select().count()
    # print(blog_json)
    print('Title: {0}'.format(blog_title), flush=True)
    print('Url: {0}'.format(blog_url), flush=True)
    print('Last Update: {0}'.format(blog_last_update), flush=True)
    print('Posts: {0}'.format(blog_posts), flush=True)

    print('Local Posts: {0}'.format(local_posts), flush=True)
    print('Local Post Contents: {0}'.format(local_all_posts_content), flush=True)
    print('Local Downloaded Post Contents: {0}'.format(local_downloaded_posts_content), flush=True)


def update_repo(repo_path, workers, timeout, force):
    db_path, configdata = get_repo_config(repo_path)
    appkey = configdata['appkey']
    blogname = configdata['tumblr_name']

    database = SqliteDatabase(db_path, journal_mode='WAL')
    database_proxy.initialize(database)

    lastpostindb = PostModel.select(fn.Max(PostModel.timestamp)).get()
    lasttimestamp = lastpostindb.timestamp

    if not lasttimestamp:
        lasttimestamp = 0
    proxies = {}
    if 'proxies' in configdata:
        proxies = configdata['proxies']
        print(proxies)

    blog_info_url_base = 'http://api.tumblr.com/v2/blog/{0}.tumblr.com/info?api_key={1}'
    blog_info_url = blog_info_url_base.format(blogname, appkey)

    inforesponse = requests.get(blog_info_url, proxies)
    response_info = inforesponse.json()
    if response_info['meta']['msg'] != 'OK' or response_info['meta']['status'] != 200:
        print('Response error: ' + response_info['meta']['msg'])
        exit()

    blog_json = response_info['response']['blog']
    blog_title = ''
    if not blog_json['title']:
        blog_title = blog_json['title']
    else:
        blog_title = blog_json['name']

    posts_count = blog_json['posts']
    print('Blog {0} has {1} posts'.format(blog_title, posts_count))
    loop_times = math.ceil(posts_count / 20)

    taskset = set()
    for i in range(0, loop_times):
        taskset.add((20 * i, 0))
    taskset = sorted(taskset, key=lambda task: task[0])
    while taskset:
        # offset, retry = taskset.pop()
        # limit = 20
        # blog_posts_url = blog_posts_url_base.format(blogname, 'photo', offset, limit, appkey)
        tasklist = []
        index = 0
        while index < workers and len(taskset) > 0:
            tasklist.append(taskset.pop())
            index += 1
        pool = ThreadPool(workers)
        func = partial(get_post_from_web, appkey, blogname, proxies, timeout, db_path, posts_count)
        results = pool.map(func, tasklist)
        pool.close()
        pool.join()

        for result, photoslist, offset, retry, latesttimestamp in results:
            if result and photoslist is not None:
                if not force and latesttimestamp <= lasttimestamp:
                    print('\nReach last update position.', flush=True)
                    exit()
                    break
            else:
                taskset.add((offset, retry + 1))
    print(' ' * 80, flush=True)
    print('All done.')


def get_post_from_web(appkey, blogname, proxies, timeout, db_path, postcount, task):
    postcontents = []
    offset = task[0]
    limit = 20
    retry = task[1]
    try:
        blog_posts_url_base = 'http://api.tumblr.com/v2/blog/{0}.tumblr.com/posts/{1}?notes_info=false&offset={2}&limit={3}&api_key={4}'
        blog_posts_url = blog_posts_url_base.format(blogname, 'photo', offset, limit, appkey)
        postsresponse = requests.get(blog_posts_url, proxies=proxies, timeout=timeout)
        response_posts = postsresponse.json()
    except (ValueError, requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout) as err:
        # print(err, flush=True)
        return False, None, offset, retry + 1, 0
    if response_posts['meta']['msg'] != 'OK' or response_posts['meta']['status'] != 200:
        # print('Response error: ' + response_posts['meta']['msg'], flush=True)
        return False, None, offset, retry + 1, 0
    posts_json = response_posts['response']['posts']
    latesttimestamp = posts_json[0]['timestamp']
    for post in posts_json:
        postid = post['id']
        posttype = post['type']
        if posttype == 'photo':
            posttypeid = 1
        elif posttype == 'video':
            posttypeid = 2
        elif posttype == 'audio':
            posttypeid = 3
        elif posttype == 'text':
            posttypeid = 4
        else:
            # Unsupported type
            continue
        tagstring = json.dumps(post['tags'])
        postmodel = PostModel.create_or_get(postid=postid,
                                            timestamp=post['timestamp'],
                                            posturl=post['post_url'],
                                            date=post['date'],
                                            posttype=posttypeid,
                                            tags=tagstring,
                                            contentfield1=post['caption']
                                            )[0]
        if posttype == 'photo':
            photos_json = post['photos']
            for photo in photos_json:
                photo_url = photo['original_size']['url']
                disassembled = urlparse(photo_url)
                filename = os.path.basename(disassembled.path)
                postcontents.append(
                        {'post': postmodel, 'posttype': 1, 'downloaded': False, 'url': photo['original_size']['url'],
                         'filename': filename})
        elif posttype == 'video':
            # Unsupported for now
            continue
        elif posttype == 'audio':
            # Unsupported for now
            continue
        elif posttype == 'text':
            # Unsupported for now
            continue
    with database_proxy.atomic():
        PostContentModel.insert_many(postcontents).execute()
    currentcount = PostModel.select().count()
    print(' ' * 80, flush=True, end='\r')
    print('[UPDATE] {0} of {1}'.format(currentcount, postcount), flush=True, end='\r')
    return True, postcontents, offset, retry, latesttimestamp


def download_repo(repo_path, workers):
    db_path, configdata = get_repo_config(repo_path)

    database = SqliteDatabase(db_path, journal_mode='WAL')
    database_proxy.initialize(database)

    proxies = {}
    if 'proxies' in configdata:
        proxies = configdata['proxies']
        print(proxies)

    print('Download starting...', flush=True)

    downloaded_posts_content = PostContentModel.select().where(PostContentModel.downloaded == True).count()
    all_posts_content = PostContentModel.select().count()

    print('{0} of {1} already downloaded.'.format(downloaded_posts_content, all_posts_content), flush=True)
    downloaditems = PostContentModel.select().where(PostContentModel.downloaded == False).limit(workers * 10).execute()
    while downloaditems:
        pool = ThreadPool(workers)
        func = partial(downloadfile, repo_path, db_path, proxies)
        pool.map(func, downloaditems)
        pool.close()
        pool.join()

        downloaditems = PostContentModel.select().where(PostContentModel.downloaded == False).limit(workers * 10).execute()

    print('\nDownload done.', flush=True)


def downloadfile(repo_path, db_path, proxies, downloaditem):
    url = downloaditem.url
    filename = downloaditem.filename
    posttype = downloaditem.posttype
    if posttype == 1:
         filepath = os.path.join(repo_path, 'photo', filename)
    elif posttype == 2:
         filepath = os.path.join(repo_path, 'video', filename)
    elif posttype == 3:
         filepath = os.path.join(repo_path, 'audio', filename)
    elif posttype == 4:
         filepath = os.path.join(repo_path, 'text', filename)

    try:
        response = requests.get(url, proxies=proxies, timeout=10)
        with open(filepath, "wb+") as targetfile:
            targetfile.write(response.content)
        query = PostContentModel.update(downloaded=True).where(PostContentModel.url == url)
        query.execute()
        downloaded_posts_content = PostContentModel.select().where(PostContentModel.downloaded).count()
        all_posts_content = PostContentModel.select().count()
        print(' ' * 80, flush=True, end='\r')
        prompt = 'Download: {0} / {1}'.format(downloaded_posts_content, all_posts_content)
        print(prompt, flush=True, end='\r')
        return url, True
    except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError, requests.exceptions.MissingSchema) as err:
        print(err, flush=True)
        return url, False


def get_repo_config(repo_path):
    repo_config_path = os.path.join(repo_path, '.tumblr')
    db_path = os.path.join(repo_config_path, 'data.db')
    config_file_path = os.path.join(repo_config_path, 'config.json')
    with open(config_file_path, 'r') as configfile:
        configdata = json.load(configfile)
    return db_path, configdata


def fileinfo(repo_path, filename):
    db_path, configdata = get_repo_config(repo_path)

    database = SqliteDatabase(db_path, journal_mode='WAL')
    database_proxy.initialize(database)
    item = PostContentModel.select(PostContentModel, PostModel).join(PostModel).where(PostContentModel.filename == filename).get()
    print(item.post.contentfield1)


def main(argv):
    args, commands = args_handler(argv)

    if commands[0] == 'init':
        if len(commands) < 3:
            print("Too few arguments!")
            exit()
        blogname = commands[1]
        repo_path = commands[2]

        init_repo(args.appkey, blogname, repo_path, args.posttype)
    elif commands[0] == 'update':
        currentpath = os.getcwd()
        print('Update repo: ' + currentpath)
        update_repo(repo_path=currentpath, workers=args.workers, timeout=10, force=args.force)
    elif commands[0] == 'status':
        currentpath = os.getcwd()
        print('Getting repo status...')
        status_repo(repo_path=currentpath, workers=args.workers, timeout=10)
    elif commands[0] == 'fileinfo':
        currentpath = os.getcwd()
        filename = commands[1]
        fileinfo(currentpath, filename)
        print('Update repo: ' + currentpath)
    elif commands[0] == 'syncstate':
        currentpath = os.getcwd()
        print('Update repo: ' + currentpath)
    elif commands[0] == 'download':
        currentpath = os.getcwd()
        download_repo(currentpath, workers=args.workers)
        print('Download repo')
    else:
        print('Invalid arguments!')
        exit()
        # print(args)


if __name__ == '__main__':
    init(autoreset=True)
    argv = sys.argv
    main(argv)
