#!/usr/bin/env python

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

setup(

    name="tumblr-dl",
    version="0.0.1",
    description="",
    author="Cryse Hillmes",
    author_email="tyk5555@hotmail.com  ",
    url="",
    packages=find_packages(),
    license="LICENSE",

    install_requires=[
        'PyTumblr',
        'requests',
        'colorama',
        'peewee'
    ],

    tests_require=[
    ]

)
